package main

import (
	"fmt"
	"github.com/spf13/cobra"
)

func NewCommand() *cobra.Command {
	c := &cobra.Command{
		Use:   "pooper [github.com/org/repo]",
		Short: "Fully-featured Pooper",
		Long:  "Scaffold a new Pooper SDK blockchain with a default directory structure",
		RunE:  doSomething,
	}

	return c
}

func doSomething(cmd *cobra.Command, args []string) error {
	fmt.Print(cmd.Name())
	fmt.Println("This is a thing?")
	return nil
}
