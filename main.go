package main

import (
	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"
	sdk "github.com/nik/starport/starport/pkg/pluginsdk"
	"os"
)

// TestPlugin poop
type TestPlugin struct {
	logger hclog.Logger
}

// OnLoad leave me alone
func (g *TestPlugin) GetManifest() sdk.PluginManifest {
	g.logger.Debug("loading plugin")

	return sdk.PluginManifest{
		ID:      "1",
		Name:    "my first",
		Author:  "nik",
		Version: "1.1",
	}
}

func (g *TestPlugin) OnLoad() error {
	g.logger.Debug("LOADING LOGIC WOO")
	return nil
}

func (g *TestPlugin) ListCommands() []sdk.Command {
	com := NewCommand()

	command := sdk.Command{
		Name:              "This is a command",
		PlaceCommandUnder: "starport account list",
		Cmd:               com,
	}

	return []sdk.Command{command}
}

var handshakeConfig = plugin.HandshakeConfig{
	ProtocolVersion:  1,
	MagicCookieKey:   "PLUGIN",
	MagicCookieValue: "z0z0z0z",
}

func main() {
	logger := hclog.New(&hclog.LoggerOptions{
		Level:      hclog.Trace,
		Output:     os.Stderr,
		JSONFormat: true,
	})

	test := &TestPlugin{
		logger: logger,
	}

	var pluginMap = map[string]plugin.Plugin{
		"starport_plugin": &sdk.StarportPlugin{Impl: test},
	}

	plugin.Serve(&plugin.ServeConfig{
		HandshakeConfig: handshakeConfig,
		Plugins:         pluginMap,
	})
}
